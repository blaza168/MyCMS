<?php
// source: C:\xampp\htdocs\cms\app\presenters/templates/Article/detail.latte

use Latte\Runtime as LR;

class Template595dff7c44 extends Latte\Runtime\Template
{
	public $blocks = [
		'content' => 'blockContent',
	];

	public $blockTypes = [
		'content' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('content', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['comment'])) trigger_error('Variable $comment overwritten in foreach on line 16');
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	function blockContent($_args)
	{
		extract($_args);
		?><h1><?php echo LR\Filters::escapeHtmlText($article->title) /* line 2 */ ?></h1>
<p><?php echo LR\Filters::escapeHtmlText($article->description) /* line 3 */ ?></p>
<p><?php echo call_user_func($this->filters->texy, $article->content) /* line 4 */ ?></p>
<p><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "article.author")) ?><a  href="<?php
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("User:detail", [$article->user->route])) ?>"><?php
		echo LR\Filters::escapeHtmlText($article->user->username) /* line 5 */ ?></a></p>
<p><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "article.category")) ?><a href="<?php
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Category:detail", [$article->category->route])) ?>"><?php
		echo LR\Filters::escapeHtmlText($article->category->name) /* line 6 */ ?></a></p>
<?php
		if ($user->isAllowed('article', 'edit')) {
			?>        <p><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Article:edit", [$article->route])) ?>"><?php
			echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "com.base.edit")) ?></a> | <a href="<?php
			echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("delete!", [$article->id])) ?>"><?php
			echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "com.base.delete")) ?></p>
<?php
		}
		if ($user->isInRole('admin') && $article->released === 0) {
?>
        <p>
            <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("release!", [$article->id])) ?>"><?php
			echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "article.release")) ?></a>
        </p>
<?php
		}
?>
<hr>
<?php
		$iterations = 0;
		foreach ($article->comments as $comment) {
			?>    <?php echo LR\Filters::escapeHtmlText($comment->content) /* line 17 */ ?>

<?php
			$iterations++;
		}
		
	}

}
