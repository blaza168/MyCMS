<?php
// source: C:\xampp\htdocs\cms\app\presenters/templates/@layout.latte

use Latte\Runtime as LR;

class Templatec03fb29af1 extends Latte\Runtime\Template
{
	public $blocks = [
		'head' => 'blockHead',
		'scripts' => 'blockScripts',
	];

	public $blockTypes = [
		'head' => 'html',
		'scripts' => 'html',
	];


	function main()
	{
		extract($this->params);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<title><?php
		if (isset($this->blockQueue["title"])) {
			$this->renderBlock('title', $this->params, function ($s, $type) {
				$_fi = new LR\FilterInfo($type);
				return LR\Filters::convertTo($_fi, 'html', $this->filters->filterContent('striphtml', $_fi, $s));
			});
			?> | <?php
		}
?>MyCMS</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
	<meta name="google" content="no-translate">
	<link rel="stylesheet" href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 15 */ ?>/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 16 */ ?>/css/style.css">
	<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('head', get_defined_vars());
?>
</head>

<body>
<?php
		if (Nette\Bridges\CacheLatte\CacheMacro::createCache($this->global->cacheStorage, 'dchzpk3wxc', $this->global->cacheStack)) {
?>
<nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<a class="navbar-brand" href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Homepage:")) ?>">MyCMS.cz</a>

	<div class="collapse navbar-collapse" id="navbarsExampleDefault">
		<ul class="navbar-nav mr-auto">
<?php
			Nette\Bridges\CacheLatte\CacheMacro::endCache($this->global->cacheStack);
		}
		?>			<li<?php if ($_tmp = array_filter([$presenter->isLinkCurrent('Homepage:default') ? 'active' : NULL, 'nav-item'])) echo ' class="', LR\Filters::escapeHtmlAttr(implode(" ", array_unique($_tmp))), '"' ?>>
				<a class="nav-link" href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Homepage:")) ?>"><?php
		echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "nav.home")) ?> <span class="sr-only">(current)</span></a>
			</li>
			<li<?php if ($_tmp = array_filter([$presenter->isLinkCurrent('Article:*') ? 'active'  : NULL, 'nav-item', 'dropdown'])) echo ' class="', LR\Filters::escapeHtmlAttr(implode(" ", array_unique($_tmp))), '"' ?>>
				<a class="nav-link dropdown-toggle"  href="#"  id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php
		echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "nav.articles")) ?></a>
				<div class="dropdown-menu" aria-labelledby="dropdown01">
<?php
		if ($user->isAllowed('article', 'write')) {
			?>					<a class="dropdown-item" href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Article:create")) ?>"><?php
			echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "nav.create")) ?></a>
<?php
		}
		if ($user->isInRole('admin')) {
			?>						<a class="dropdown-item" href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Article:admin")) ?>"><?php
			echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "nav.administration")) ?></a>
<?php
		}
		?>					<a class="dropdown-item" href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Article:list")) ?>"><?php
		echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "nav.latest")) ?></a>
				</div>
			</li>
			<li<?php if ($_tmp = array_filter([$presenter->isLinkCurrent('Category:*') ? 'active'  : NULL, 'nav-item', 'dropdown'])) echo ' class="', LR\Filters::escapeHtmlAttr(implode(" ", array_unique($_tmp))), '"' ?>>
				<a class="nav-link dropdown-toggle"  href="#"   id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php
		echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "nav.category")) ?></a>
				<div class="dropdown-menu" aria-labelledby="dropdown01">
<?php
		if ($user->isAllowed('category', 'create')) {
			?>						<a class="dropdown-item" href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Category:create")) ?>"><?php
			echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "nav.create")) ?></a>
<?php
		}
		?>					<a class="dropdown-item" href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Category:list")) ?>"><?php
		echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "nav.list")) ?></a>
				</div>
			</li>
	<!--		<li class="nav-item">
				<a class="nav-link disabled" href="#">Disabled</a>
			</li> -->

<?php
		if (!$user->isLoggedIn()) {
			?>			<li<?php if ($_tmp = array_filter([$presenter->isLinkCurrent('Sign:*') ? 'active'  : NULL, 'nav-item', 'dropdown'])) echo ' class="', LR\Filters::escapeHtmlAttr(implode(" ", array_unique($_tmp))), '"' ?>>
				<a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php
			echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "nav.sign")) ?></a>
				<div class="dropdown-menu" aria-labelledby="dropdown01">
					<a class="dropdown-item" href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Sign:in")) ?>"><?php
			echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "nav.in")) ?></a>
					<a class="dropdown-item" href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Sign:up")) ?>"><?php
			echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "nav.up")) ?></a>
				</div>
			</li>
<?php
		}
		else {
			?>				<li<?php if ($_tmp = array_filter([$presenter->isLinkCurrent('User:*') ? 'active'  : NULL, 'nav-item', 'dropdown'])) echo ' class="', LR\Filters::escapeHtmlAttr(implode(" ", array_unique($_tmp))), '"' ?>>
					<a class="nav-link dropdown-toggle"  href="#"  id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php
			echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "nav.profile")) ?></a>
					<div class="dropdown-menu" aria-labelledby="dropdown01">
						<a class="dropdown-item" href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("User:detail", [$userEntity->route])) ?>"><?php
			echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "nav.myProfile")) ?></a>
						<a class="dropdown-item" href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("User:settings")) ?>"><?php
			echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "nav.settings")) ?></a>
					</div>
				</li>
				<li>
					<a class="nav-link" href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Logout!")) ?>"><?php
			echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "nav.logout")) ?></a>
				</li>
<?php
		}
?>
		</ul>
<?php
		$form = $_form = $this->global->formsStack[] = $this->global->uiControl["searchForm"];
		?>		<form class="form-inline my-2 my-lg-0"<?php
		echo Nette\Bridges\FormsLatte\Runtime::renderFormBegin(end($this->global->formsStack), array (
		'class' => NULL,
		), false) ?>>
			<input class="form-control mr-sm-2" placeholder="<?php echo LR\Filters::escapeHtmlAttr(call_user_func($this->filters->translate, "form.search.submit")) ?>" type="text"<?php
		$_input = end($this->global->formsStack)["text"];
		echo $_input->getControlPart()->addAttributes(array (
		'class' => NULL,
		'placeholder' => NULL,
		'type' => NULL,
		))->attributes() ?>>
			<button class="btn btn-outline-success my-2 my-sm-0" type="submit" <?php
		$_input = end($this->global->formsStack)["submit"];
		echo $_input->getControlPart()->addAttributes(array (
		'class' => NULL,
		'type' => NULL,
		))->attributes() ?>><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "form.search.submit")) ?></button>
<?php
		echo Nette\Bridges\FormsLatte\Runtime::renderFormEnd(array_pop($this->global->formsStack), false);
?>		</form>
	</div>
</nav>

<?php
		$iterations = 0;
		foreach ($flashes as $flash) {
			?>	<div class="alert alert-<?php echo LR\Filters::escapeHtmlAttr($flash->type) /* line 96 */ ?> alert-dismissible fade show" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<?php echo $flash->message /* line 100 */ ?>

	</div>
<?php
			$iterations++;
		}
?>

	<div class="container">
<?php
		$this->renderBlock('content', $this->params, 'html');
?>
	</div>

<?php
		$this->renderBlock('scripts', get_defined_vars());
?>
</body>
</html>
<?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['flash'])) trigger_error('Variable $flash overwritten in foreach on line 96');
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		Nette\Bridges\CacheLatte\CacheMacro::initRuntime($this);
		
	}


	function blockHead($_args)
	{
		
	}


	function blockScripts($_args)
	{
		extract($_args);
?>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<script src="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 109 */ ?>/js/bootstrap.min.js"></script>
	<script src="https://nette.github.io/resources/js/netteForms.min.js"></script>
	<script src="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 111 */ ?>/js/main.js"></script>
<?php
	}

}
