<?php
// source: C:\xampp\htdocs\cms\app\presenters/templates/Homepage/default.latte

use Latte\Runtime as LR;

class Template9fc735f39f extends Latte\Runtime\Template
{
	public $blocks = [
		'content' => 'blockContent',
	];

	public $blockTypes = [
		'content' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('content', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['info'])) trigger_error('Variable $info overwritten in foreach on line 5');
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	function blockContent($_args)
	{
		extract($_args);
?>

<?php
		if (count($informations) > 0) {
?>
<table class="infoTable">
<?php
			$iterations = 0;
			foreach ($informations as $info) {
				$data = explode('|', $info->content);
?>
    <tr>
        <td><p>
<?php
				$this->global->switch[] = ($data[0]);
				if (FALSE) {
				}
				elseif (end($this->global->switch) === ('register_user')) {
					?>                     <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("User:detail", [$data[2]])) ?>"><?php
					echo LR\Filters::escapeHtmlText($data[1]) /* line 11 */ ?></a><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, $data[3])) ?>

<?php
				}
				elseif (end($this->global->switch) === ('article')) {
					?>                    <a href="#"><?php echo LR\Filters::escapeHtmlText($data[1]) /* line 13 */ ?></a><?php
					echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, $data[3])) ?><a href="<?php
					echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Article:detail", [$data[5]])) ?>"><?php
					echo LR\Filters::escapeHtmlText($data[4]) /* line 13 */ ?></a>
<?php
				}
				elseif (end($this->global->switch) === ('category')) {
					?>                    <a href="#"><?php echo LR\Filters::escapeHtmlText($data[1]) /* line 15 */ ?></a><?php
					echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, $data[3])) ?><a href="<?php
					echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Category:detail", [$data[5]])) ?>"><?php
					echo LR\Filters::escapeHtmlText($data[4]) /* line 15 */ ?></a>
<?php
				}
				array_pop($this->global->switch) ?>

                <small><?php echo LR\Filters::escapeHtmlText($info->time->format('H:i j.n')) /* line 18 */ ?></small>
            </p></td>
    </tr>
<?php
				$iterations++;
			}
?>
</table>
<?php
		}
		else {
			?>    <h1 style="text-align: center; margin-top: 50px;"><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "com.homepage.noInfo")) ?></h1>
<?php
		}
		
	}

}
