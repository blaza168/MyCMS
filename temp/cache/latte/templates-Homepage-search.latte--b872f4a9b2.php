<?php
// source: C:\xampp\htdocs\cms\app\presenters/templates/Homepage/search.latte

use Latte\Runtime as LR;

class Templateb872f4a9b2 extends Latte\Runtime\Template
{
	public $blocks = [
		'content' => 'blockContent',
		'_users' => 'blockUsers',
		'_categories' => 'blockCategories',
		'_articles' => 'blockArticles',
		'scripts' => 'blockScripts',
	];

	public $blockTypes = [
		'content' => 'html',
		'_users' => 'html',
		'_categories' => 'html',
		'_articles' => 'html',
		'scripts' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('content', get_defined_vars());
		$this->renderBlock('scripts', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['searchedUser'])) trigger_error('Variable $searchedUser overwritten in foreach on line 10');
		if (isset($this->params['category'])) trigger_error('Variable $category overwritten in foreach on line 23');
		if (isset($this->params['article'])) trigger_error('Variable $article overwritten in foreach on line 36');
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	function blockContent($_args)
	{
		extract($_args);
		$count->users = $count->users[0][1];
		$count->articles = $count->articles[0][1];
		$count->categories = $count->categories[0][1];
		if ($count->users > 0 || $count->categories > 0 || $count->articles > 0) {
			if ($count->users > 0) {
				?>            <h1><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "com.search.users")) ?></h1>
            <hr>
<div id="<?php echo htmlSpecialChars($this->global->snippetDriver->getHtmlId('users')) ?>"><?php $this->renderBlock('_users', $this->params) ?></div><?php
			}
			if ($count->categories > 0) {
				?>            <h1><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "com.search.categories")) ?></h1>
            <hr>
<div id="<?php echo htmlSpecialChars($this->global->snippetDriver->getHtmlId('categories')) ?>"><?php $this->renderBlock('_categories', $this->params) ?></div><?php
			}
			if ($count->articles > 0) {
				?>            <h1><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "com.search.articles")) ?></h1>
            <hr>
<div id="<?php echo htmlSpecialChars($this->global->snippetDriver->getHtmlId('articles')) ?>"><?php $this->renderBlock('_articles', $this->params) ?></div><?php
			}
		}
		else {
			?>        <h1><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->translate, "com.search.notFound")) ?></h1>
<?php
		}
		
	}


	function blockUsers($_args)
	{
		extract($_args);
		$this->global->snippetDriver->enter("users", "static");
		$iterations = 0;
		foreach ($result->users as $searchedUser) {
			?>                    <h2><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("User:detail", [$searchedUser->route])) ?>"><?php
			echo LR\Filters::escapeHtmlText($searchedUser->username) /* line 11 */ ?></a></h2>
<?php
			ob_start(function () {});
			?>                    <p><?php
			ob_start();
			echo LR\Filters::escapeHtmlText($searchedUser->settings->description) /* line 12 */;
			$this->global->ifcontent = ob_get_flush();
?></p>
<?php
			if (rtrim($this->global->ifcontent) === "") ob_end_clean();
			else echo ob_get_clean();
			$iterations++;
		}
		$this->global->snippetDriver->leave();
		
	}


	function blockCategories($_args)
	{
		extract($_args);
		$this->global->snippetDriver->enter("categories", "static");
		$iterations = 0;
		foreach ($result->categories as $category) {
			?>                    <h2><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Category:detail", [$category->route])) ?>"><?php
			echo LR\Filters::escapeHtmlText($category->name) /* line 24 */ ?></a></h2>
                    <p><?php echo LR\Filters::escapeHtmlText($category->description) /* line 25 */ ?></p>
<?php
			$iterations++;
		}
		$this->global->snippetDriver->leave();
		
	}


	function blockArticles($_args)
	{
		extract($_args);
		$this->global->snippetDriver->enter("articles", "static");
		$iterations = 0;
		foreach ($result->articles as $article) {
			?>                    <h2><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Article:detail", [$article->route])) ?>"><?php
			echo LR\Filters::escapeHtmlText($article->title) /* line 37 */ ?></a></h2>
                    <p><?php echo LR\Filters::escapeHtmlText($article->description) /* line 38 */ ?></p>
<?php
			$iterations++;
		}
		$this->global->snippetDriver->leave();
		
	}


	function blockScripts($_args)
	{
		extract($_args);
		$this->renderBlockParent('scripts', get_defined_vars());
		?>    <script src="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 51 */ ?>/js/nette.ajax.js"></script>
    <script>
        function btnClick(text, count, entity){
            $.nette.ajax({
                type: "POST",
                dateType: "json",
                url: <?php echo LR\Filters::escapeJs($this->global->uiControl->link("Homepage:search")) ?>,
                data: { count: count ,text: text, entity: entity }
            });
        }
    </script>
<?php
	}

}
