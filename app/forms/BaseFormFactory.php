<?php

namespace App\Forms;

use App\Model\Facades\UserFacade;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;
use App\Model\Facades\InfoFacade;
use Nette\Security\User;

abstract class BaseFormFactory
{
    
    /**
     * @var Translator
     */
    protected $translator;

    /** @var User */
    protected $user;

    /**
     * @var InfoFacade
     */
    protected $infoFacade;

    /**
     * @var UserFacade
     */
    protected $userFacade;

    /**
     * BaseFormFactory constructor.
     * @param InfoFacade $infoFacade
     * @param Translator $translator
     */
    public function __construct(UserFacade $userFacade,User $user,InfoFacade $infoFacade, Translator $translator)
    {
        $this->userFacade = $userFacade;
        $this->user = $user;
        $this->translator = $translator;
        $this->infoFacade = $infoFacade;
    }
}