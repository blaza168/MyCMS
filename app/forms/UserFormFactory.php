<?php

namespace App\Forms;

use Nette\InvalidArgumentException;
use Nette\Utils\ArrayHash;
use Czubehead\BootstrapForms\BootstrapForm as BForm;

final class UserFormFactory extends BaseFormFactory
{

    /**
     * @return BForm form for user settings
     */
    function settingsForm()
    {
        $form = new BForm();
        $form->addTextArea('description')
            ->setRequired('form.user.descRequired')
            ->setAttribute('rows', 3)
            ->setAttribute('placeholder', 'form.user.desc')
            ->addRule(Form::MAX_LENGTH, 'form.user.descLength', 500);

        $form->addSubmit('submit', 'form.user.submit');

        $form->setTranslator($this->translator);

        $form->addProtection('form.protectionError');

        $form->onSuccess[] = [$this, 'settingsSucceeded'];
        return $form;
    }

    /**
     * Called after succesfully validate settings form
     * @param BForm $form
     * @param ArrayHash $vals
     */
    function settingsSucceeded(Form $form, ArrayHash $vals)
    {
        $this->userFacade->editSettings($vals, $this->user->getId());
    }

    private function getOptions()
    {
        return array(
            '1 year' => 'user.date.year',
            'permanent' => 'user.date.permanent',
            '1 week' => 'user.date.week',
            '1 day' => 'user.date.day'
        );
    }

    /**
     * @return BForm form with bootstrap design
     */
    function banForm()
    {
        $form = new BForm();

        $form->addHidden('userId');

        $form->addSelect('time', 'form.user.time', $this->getOptions());

        $form->addSubmit('submit', 'form.user.ban');

        $form->addTextArea('reason', 'form.user.reason')
            ->setRequired(false)
            ->setAttribute('row', 4)
            ->addRule(BForm::MAX_LENGTH, 'form.user.reasonLength', 500);

        $form->setTranslator($this->translator);

        $form->addProtection('form.protectionError');

        $form->onSuccess[] = [$this, 'banSucceeded'];

        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $vals
     */
    function banSucceeded(Form $form, ArrayHash $vals)
    {
        try{
            $this->userFacade->banUser($vals);
        } catch(InvalidArgumentException $e) {
            $form->addError($this->translator->translate('form.user.notFound'));
        }
    }
}