<?php

namespace App\Forms;

use App\Model\Facades\ArticleFacade;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Nette\Utils\ArrayHash;


final class SignFormFactory extends BaseFormFactory
{


    /**
     * @return Form
     */
    public function signInForm()
    {
        $form = new Form();

        $form->addText('username')
            ->setRequired('form.usernameRequired')
            ->addRule(Form::MAX_LENGTH, 'form.usernameMaxLength', 20);

        $form->addPassword('password')
            ->setRequired('form.passwordRequired');

        $form->addCheckbox('remember');

        $form->addProtection('form.protectionError');

        $form->addSubmit('login');

        $form->onSuccess[] = [$this, 'signInSucceeded'];

        $form->setTranslator($this->translator);

        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $vals
     */
    public function signInSucceeded(Form $form, $vals)
    {
        try {
            $this->user->login($vals->username, $vals->password);
            if($vals->remember)
                $this->user->setExpiration('7 days', FALSE);
            else
                $this->user->setExpiration('30 minutes', TRUE);
        } catch (AuthenticationException $e) {
            $form['username']->addError($e->getMessage());
        }

    }

    /**
     * @return Form
     */
    function signUpForm(){
        $form = new Form();

        $form->addText('username')
            ->setRequired('form.usernameRequired')
            ->addRule(Form::MAX_LENGTH, 'form.usernameMaxLength', 20);

        $form->addEmail('email')
            ->addRule(Form::EMAIL, 'form.emailValidation')
            ->setRequired('form.emailRequired');

        $form->addPassword('password')
            ->addRule(Form::MIN_LENGTH, 'form.passwordLength', 5)
            ->setRequired('form.passwordRequired');

        $form->addPassword('passwordAgain')
            ->addRule(Form::EQUAL, 'form.passwordsNotSame', $form['password'])
            ->setRequired('form.passwordAgainRequired')
            ->setOmitted(true);

        $form->addSubmit('register');

        $form->onSuccess[] = [$this , 'signUpSucceeded'];

        $form->setTranslator($this->translator);

        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $vals
     */
    function signUpSucceeded(Form $form, $vals)
    {
        $vals->ip = $form->getPresenter()->context->getService('httpRequest')->getRemoteAddress();
        $vals->route = ArticleFacade::createRoute($vals->username);
        try{
            $this->userFacade->register($vals);
        } catch(UniqueConstraintViolationException $e) {
            if (strpos($e->getMessage(), 'username'))
                $form['username']->addError($this->translator->translate('form.usernameDuplicate'));
            else
                $form['username']->addError($this->translator->translate('form.emailDuplicate'));
        }
        $this->infoFacade->registeredUser($vals->username, $vals->route);
        $this->user->login($vals->username, $vals->password);
    }
}