<?php
/**
 * Created by PhpStorm.
 * User: Pokoj
 * Date: 03.08.2017
 * Time: 11:16
 */

namespace App\Forms;

use Kdyby\Translation\Translator;
use Nette\Application\UI\Form;

final class searchFormFactory
{
    /** @var Translator */
    protected $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    function searchForm()
    {
        $form = new Form();
        $form->addText('text')
            ->addRule(Form::MAX_LENGTH, 'form.search.textLength', 100)
            ->setRequired('form.search.textRequired');

        $form->addSubmit('submit');

        $form->setTranslator($this->translator);

        return $form;
    }

}