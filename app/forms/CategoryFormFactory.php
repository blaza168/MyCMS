<?php

namespace App\Forms;


use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use App\Model\Facades\CategoryFacade;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use App\Model\Facades\UserFacade;
use App\Model\Facades\ArticleFacade;
use App\Model\Facades\InfoFacade;
use Nette\Neon\Exception;
use Nette\Security\User;
use Kdyby\Translation\Translator;

final class CategoryFormFactory extends BaseFormFactory
{
    /** @var CategoryFacade  */
    protected $categoryFacade;

    public function __construct(CategoryFacade $categoryFacade,UserFacade $userFacade, User $user, InfoFacade $infoFacade, Translator $translator, ArticleFacade $articleFacade)
    {
        parent::__construct($userFacade, $user, $infoFacade, $translator);
        $this->categoryFacade = $categoryFacade;
    }

    /**
     * @return string user´s name
     */
    private function getUser()
    {
        return $this->userFacade->getUser($this->user->getId());
    }

    /**
     * @return Form
     */
    public function createCategoryForm()
    {
        $form = new Form();

        $form->addText('name')
            ->addRule(Form::MAX_LENGTH, 'form.category.nameRequired', 20)
            ->setRequired('form.category.nameRequired');

        $form->addHidden('categoryId')
            ->setRequired(false);

        $form->addText('route')
			->setRequired(false)
			->addRule(Form::MAX_LENGTH, 'form.article.routeLength', 20);

        $form->addTextArea('description')
            ->setRequired('form.category.descriptionRequired')
            ->addRule(Form::MAX_LENGTH, 'form.category.descriptionLength', 150);

        $form->addSubmit('submit');

        $form->setTranslator($this->translator);

        $form->addProtection('form.protectionError');

        $form->onSuccess[] = [$this, 'createCategorySucceeded'];

        return $form;
    }

    /**
     * @param Form $form
     * @param $vals
     */
    public function createCategorySucceeded(Form $form, $vals)
    {
    	$user = $this->getUser();
        if(empty($vals->categoryId)){
        	if(empty($vals->route))
        		$vals->route = ArticleFacade::createRoute($vals->name);
            try{
                $this->categoryFacade->createCategory($vals);
                $this->infoFacade->createdCategory($user->username, $user->id, $vals->name, $vals->route);
            } catch(UniqueConstraintViolationException $e){
                $form['name']->addError($this->translator->translate('exception.category.nameDuplicate'));
            }
        }
        else{
            try{
                $this->categoryFacade->editCategory($vals);
                $this->infoFacade->editedCategory($user->username, $user->id, $vals->name, $vals->route);
            } catch(UniqueConstraintViolationException $e){
            	if(strpos($e->getMessage(), 'name') !== false)
                	$form['name']->addError($this->translator->translate('exception.category.nameDuplicate'));
            	else
            		$form['name']->addError($this->translator->translate('exception.route'));
            } catch(BadRequestException $e){
                $form['name']->addError($this->translator->translate('exception.category.badRequest'));
            }
        }
    }

}