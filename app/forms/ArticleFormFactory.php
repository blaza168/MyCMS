<?php

namespace App\Forms;


use App\Model\Facades\ArticleFacade;
use App\Model\Facades\CategoryFacade;
use Doctrine\DBAL\Exception\NonUniqueFieldNameException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Nette\Application\UI\Form;
use App\Model\Facades\UserFacade;
use App\Model\Facades\InfoFacade;
use Kdyby\Translation\Translator;
use Nette\Security\User;
use Symfony\Component\Config\Definition\Exception\Exception;

final class ArticleFormFactory extends BaseFormFactory
{

    /**
     * @var ArticleFacade
     */
    protected $articleFacade;

    /**
     * @var CategoryFacade
     */
    protected $categoryFacade;

    /**
     * ArticleFormFactory constructor.
     * @param UserFacade $userFacade
     * @param User $user
     * @param InfoFacade $infoFacade
     * @param Translator $translator
     * @param ArticleFacade $articleFacade
     */
    public function __construct(CategoryFacade $categoryFacade,UserFacade $userFacade, User $user, InfoFacade $infoFacade, Translator $translator, ArticleFacade $articleFacade)
    {
        parent::__construct($userFacade, $user, $infoFacade, $translator);
        $this->articleFacade = $articleFacade;
        $this->categoryFacade = $categoryFacade;
    }

    public function createArticleForm()
    {
        $form = new Form();
        $form->addText('title')
            ->setRequired('form.article.titleRequired')
            ->addRule(Form::MAX_LENGTH, 'form.article.titleLength' , 20);

        $form->addText('route')
            ->setRequired(FALSE)
            ->addRule(Form::MAX_LENGTH, 'form.article.routeLength', 40);

        $form->addSelect('category', NULL, $this->articleFacade->getCategories())
                ->setRequired('form.article.selectRequired');

        $form->addTextArea('description')
            ->setAttribute('cols, 2')
            ->addRule(Form::MAX_LENGTH, 'form.article.descriptionLength', 150)
            ->setRequired('form.article.descriptionRequired');

        $form->addTextArea('content')
            ->setAttribute('class', 'form-textArea')
            ->setAttribute('placeholder', 'form.article.content')
            ->setAttribute('id', 'inputContent')
            ->setRequired('form.article.contentRequired')
            ->addRule(Form::MAX_LENGTH, 'form.article.descriptionLength', 10000);

        $form->addHidden('articleId')
            ->setRequired(false);


        $form->addProtection('form.protectionError');

        $form->addSubmit('submit');

        $form->setTranslator($this->translator);

        $form->onSuccess[] = [$this, 'createArticleSucceeded'];

        return $form;
    }

    public function createArticleSucceeded(Form $form, $vals)
    {
        $user = $this->userFacade->getUser($this->user->getId());
        if(empty($vals->route))
            $vals->route = ArticleFacade::createRoute($vals->title);
        if(empty($vals->articleId))
        {
            try{
                $this->articleFacade->createArticle($vals, $user, $this->categoryFacade->getCategoryById($vals->category));
                if($this->user->isInRole('admin'))
                    $this->infoFacade->createdArticle($user->username, $user->id, $vals->title, $vals->route);
            } catch(UniqueConstraintViolationException $e){
                $form['title']->addError($this->translator->translate('exception.article.titleDuplicate'));
            }

        }
        else{
            try{
                $this->articleFacade->editArticle($vals);
                $this->infoFacade->editedArticle($user->username, $user->id, $vals->title, $vals->route);
            } catch(NonUniqueFieldNameException $e){
                $form['title']->addError($this->translator->translate('exception.article.titleDuplicate'));
            }
        }
    }
}