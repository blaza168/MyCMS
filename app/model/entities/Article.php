<?php
/**
 * Created by PhpStorm.
 * User: Honza
 * Date: 05.07.2017
 * Time: 13:54
 */

namespace App\Model\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * Class Article
 * @package App\Model\Entities
 * @ORM\Entity
 * @ORM\Table(name="articles")
 */
class Article extends BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @ORM\Column(type="integer")
     */
    public $released;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="articles")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    public $category;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="articles")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    public $user;

    /**
     * @ORM\Column(type="string", length=150)
     */
    public $description;

    /**
     * @ORM\Column(type="string", length=10000)
     */
    public $content;

    /**
     * @ORM\Column(type="datetime")
     */
    public $created_at;

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="article")
     */
    public $comments;

    /**
     * @ORM\Column(type="string", length=20, unique=true)
     */
    public $route;

    /**
     * @ORM\Column(type="string", length=20)
     */
    public $title;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
    }
}