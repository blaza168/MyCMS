<?php
/**
 * Created by PhpStorm.
 * User: Honza
 * Date: 05.07.2017
 * Time: 13:47
 */

namespace App\Model\Entities;

use App\Model\Entities\Article;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * Class Category
 * @package App\Model\Entities
 * @ORM\Entity
 * @ORM\Table(name="categories")
 */
class Category extends BaseEntity
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    public $description;

    /**
     * @ORM\Column(type="string", length=20, unique=true)
     */
    public $name;

    /**
     * @ORM\Column(type="string", length=40, unique=true)
     */
    public $route;

    /**
     * @ORM\OneToMany(targetEntity="Article", mappedBy="category", cascade={"remove"})
     */
    public $articles;

    public function __construct()
    {
        $this->articles = new ArrayCollection();
    }

    function addArticle(Article $article)
    {
        $this->articles[] = $article;
        $article->category = $this;
    }
}