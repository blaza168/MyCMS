<?php
/**
 * Created by PhpStorm.
 * User: Honza
 * Date: 05.07.2017
 * Time: 9:58
 */

namespace App\Model\Entities;

use App\Model\Facades\Authorizator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * Class User
 * @package App\Model\Entities
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends BaseEntity
{
    const ROLE_USER = 'user',
        ROLE_MOD = 'mod',
        ROLE_ADMIN = 'admin';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    public $username;

    /**
     * @ORM\Column(type="string", length=100)
     */
    public $password;

    /**
     * @ORM\Column(type="string", length=20, unique=true)
     */
    public $route;

    /**
     * @ORM\Column(type="string", length=50)
     */
    public $email;

    /**
     * @ORM\Column(type="string", length=5)
     */
    public $role;

    /**
     * @ORM\Column(type="string", length=17)
     */
    public $ip;

    /**
     * @ORM\OneToOne(targetEntity="Ban", mappedBy="user", cascade={"remove"})
     */
    public $ban;

    /**
     * @ORM\OneToOne(targetEntity="UserSettings", mappedBy="user", cascade={"remove"})
     */
    public $settings;

    /**
     * @ORM\OneToMany(targetEntity="Article", mappedBy="user", cascade={"remove"})
     */
    public $articles;

    public function __construct()
    {
        $this->articles = new ArrayCollection();
    }

    function isAdmin()
    {
        return $this->role === self::ROLE_ADMIN;
    }

    function addArticle(Article $article)
    {
        $this->articles[] = $article;
        $article->user = $this;
    }
}