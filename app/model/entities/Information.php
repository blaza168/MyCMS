<?php

namespace App\Model\Entities;

use Kdyby\Doctrine\Entities\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Information
 * @package App\Model\Entities
 * @ORM\Entity
 * @ORM\Table(name="info")
 */
class Information extends BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @ORM\Column(type="datetime", name="datetime")
     */
    public $time;

    /**
     * @ORM\Column(type="string", length=150)
     */
    public $content;
}