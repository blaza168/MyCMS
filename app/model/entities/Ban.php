<?php
/**
 * Created by PhpStorm.
 * User: Honza
 * Date: 05.07.2017
 * Time: 13:25
 */

namespace App\Model\Entities;

use Kdyby\Doctrine\Entities\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Ban
 * @package App\Model\Entities
 * @ORM\Entity
 * @ORM\Table(name="bans")
 */
class Ban extends BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="ban")
     */
    public $user;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    protected $permanent = 0;

    /**
     * @return bool
     */
    public function isPermanent()
    {
        return $this->permanent === 1;
    }

    /**
     * @ORM\Column(name="ban_until", type="datetime", nullable=true)
     */
    public $banTime;

    /**
     * @ORM\Column(type="string", length=500)
     */
    public $reason;
}