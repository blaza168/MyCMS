<?php
/**
 * Created by PhpStorm.
 * User: Honza
 * Date: 05.07.2017
 * Time: 17:22
 */

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * Class Comment
 * @package App\Model\Entities
 * @ORM\Entity
 * @ORM\Table(name="comments")
 */
class Comment extends BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="comments")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    public $article;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    public $author;

    /**
     * @ORM\Column(type="content", length=300)
     */
    public $content;

    /**
     * @ORM\Column(type="datetime")
     */
    public $date;
}