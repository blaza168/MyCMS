<?php
/**
 * Created by PhpStorm.
 * User: Honza
 * Date: 05.07.2017
 * Time: 13:36
 */

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class UserSettings
 * @package App\Model\Entities
 * @ORM\Entity
 * @ORM\Table(name="settings")
 */
class UserSettings
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="settings")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    public $user;

    /**
     * @ORM\Column(type="string", length=200 , nullable=true)
     */
    public $description;

}