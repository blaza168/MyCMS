<?php
/**
 * Created by PhpStorm.
 * User: Honza
 * Date: 05.08.2017
 * Time: 20:22
 */

namespace App\Model\Queries;


use Kdyby;
use Kdyby\Doctrine\QueryObject;
use Kdyby\Doctrine\QueryBuilder;

final class SearchQuery extends QueryObject
{
	/** @var array  */
	private $selects = array();

	/**
	 * @param \Kdyby\Persistence\Queryable $repository
	 * @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder
	 */
/*	protected function doCreateQuery(Kdyby\Persistence\Queryable $repository)
	{
		$qb = $repository->createQueryBuilder();

		foreach ($this->selects as $select)
			$select($qb);

		return $qb;
	}

	public function selectCategories($name, $offset = 0, $limit = 5)
	{
		$this->selects[] = function (QueryBuilder $qb) use ($offset, $limit, $name){
			$qb->select('PARTIAL c.{id, description, name} AS categories')
				->from('App\Model\Entities\Category', 'c')
				->add('where', $qb->expr()->like('c.name', ':name'))
				->setFirstResult($offset)
				->setParameter('name', "%{$name}%")
				->setMaxResults($limit);
		};
		$this->addCategoriesCount($name);
	}

	private function addCategoriesCount($name)
	{
		$this->selects[] = function (QueryBuilder $qb) use ($name){
			$count = $qb->getEntityManager()->createQueryBuilder()
				->select('COUNT(c.id)')
				->from('App\Model\Entities\Category', 'c')
				->add('where', $qb->expr()->like('c.name', ':name'))
				->setParameter('name', "%{$name}%");

			$qb->addSelect("({$count}) AS cCount");
		};
	}

	public function selectArticles($title, $offset = 0, $limit = 5)
	{
		$this->selects[] = function (QueryBuilder $qb) use ($offset, $limit, $title){
			$qb->select('PARTIAL a.{id, description, title} AS articles')
				->from('App\Model\Entities\Article', 'a')
				->add('where', $qb->expr()->like('a.title', ':title'))
				->setFirstResult($offset)
				->setParameter('username', "%{$title}%")
				->setMaxResults($limit);
		};
		$this->addArticlesCount($title);
	}

	private function addArticlesCount($title)
	{
		$this->selects[] = function (QueryBuilder $qb) use ($title) {
			$count = $qb->getEntityManager()->createQueryBuilder()
				->select('COUNT(a.id)')
				->from('App\Model\Entities\Article', 'a')
				->add('where', $qb->expr()->like('a.title', ':title'))
				->setParameter('title', "%{$title}%");
			$qb->addSelect("({$count}) AS aCount");
		};
	}

	public function selectUsers($username, $offset = 0, $limit = 5)
	{
		$this->selects[] = function (QueryBuilder $qb) use ($offset, $limit, $username){
			$qb->select('PARTIAL u.{id, settings, username} AS users')
				->addSelect('PARTIAL s.{id, description}')
				->from('App\Model\Entities\User', 'u')
				->from('u.settings', 's')
				->add('where', $qb->expr()->like('u.username', ':username'))
				->setFirstResult($offset)
				->setParameter('username', "%{$username}%")
				->setMaxResults($limit);
		};
		$this->addUsersCount($username);
	}

	private function addUsersCount($username)
	{
		$this->selects[] = function (QueryBuilder $qb) use ($username){
				$count = $qb->getEntityManager()->createQueryBuilder()
					->select('COUNT(u.id)')
					->from('App\Model\Entities\User', 'u')
					->add('where', $qb->expr()->like('u.username', ':username'))
					->setParameter('username', "%{$username}%");
				$qb->addSelect("({$count}) AS uCount");
		};
	}*/
}