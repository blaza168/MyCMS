<?php
/**
 * Created by PhpStorm.
 * User: Honza
 * Date: 06.07.2017
 * Time: 10:01
 */

namespace App\Model\Queries;

use Kdyby\Doctrine\QueryObject;
use Kdyby\Persistence\Queryable;
use Doctrine\ORM\QueryBuilder;
use App\Model\Entities\Information;

final class InfoQuery extends QueryObject
{

    /** @var array */
    protected $filters = [];

    /**
     * @param Queryable $repository
     * @return QueryBuilder
     */
    protected function doCreateQuery(Queryable $repository)
    {
        $qb = $repository->createQueryBuilder()
            ->select('i')
            ->from(Information::class, 'i')
            ->addOrderBy('i.time', 'DESC');

        foreach ($this->filters as $filter) {
            $filter($qb);
        }
        return $qb;
    }


    public function getLastInfo()
    {
        $this->filters[] = function (QueryBuilder $qb) {
            $qb->addOrderBy('i.time', 'DESC');
        };
    }
}