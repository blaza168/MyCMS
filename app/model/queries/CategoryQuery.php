<?php

namespace App\Model\Queries;


use App\Model\Entities\Article;
use Doctrine\ORM\QueryBuilder;
use Kdyby;
use Kdyby\Doctrine\QueryObject;
use App\Model\Entities\Category;

final class CategoryQuery extends QueryObject
{
    /** @var array  */
    private $filters = [];

    /**
     * @param \Kdyby\Persistence\Queryable $repository
     * @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder
     */
    protected function doCreateQuery(Kdyby\Persistence\Queryable $repository)
    {
        $qb = $repository->createQueryBuilder()
                ->addSelect('c')
                ->from(Category::class, 'c')
                ->addOrderBy('c.name', 'ASC');

        foreach ($this->filters as $filter) $filter($qb);

        return $qb;
    }

    public function addArticlesCount()
    {
        $this->filters[] = function (QueryBuilder $qb){
        $count = $qb->getEntityManager()->createQueryBuilder()
            ->select('COUNT(a.id)')
            ->from(Article::class, 'a')
            ->andWhere('a.released = 1')
            ->andWhere('a.category = c');

        $qb->addSelect("({$count}) AS articlesCount");
        };
    }
}