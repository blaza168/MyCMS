<?php
/**
 * Created by PhpStorm.
 * User: Honza
 * Date: 09.07.2017
 * Time: 13:35
 */

namespace App\Model\Queries;

use App\Model\Entities\Article;
use Kdyby;
use Kdyby\Doctrine\QueryObject;
use Doctrine\ORM\QueryBuilder;
use Kdyby\Persistence\Queryable;

final class ArticleQuery extends QueryObject
{
    /** @var array  */
    private $filters = [];

    /**
     * @param Queryable $repository
     * @return \Doctrine\ORM\Query
     */
    protected function doCreateQuery(Queryable $repository)
    {
        $qb = $repository->createQueryBuilder()
            ->select('a')
            ->from(Article::class, 'a')
            ->addOrderBy('a.created_at', 'DESC');

        foreach ($this->filters as $filter) $filter($qb);

        return $qb;
    }

    public function onlyReleased()
    {
        $this->filters[] = function (QueryBuilder $qb){
             $qb->andWhere('a.released = 1');
        };

       // return $this;
    }

    public function onlyNoReleased()
    {
        $this->filters[] = function (QueryBuilder $qb){
            $qb->andWhere('a.released = 0');
        };
    }
}