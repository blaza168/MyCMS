<?php
/**
 * Created by PhpStorm.
 * User: Honza
 * Date: 09.07.2017
 * Time: 20:20
 */

namespace App\Model\Facades;

use App\Model\Entities\Category;
use App\Model\Queries\CategoryQuery;
use Nette\Application\BadRequestException;
use Nette\InvalidArgumentException;
use Nette\Utils\ArrayHash;

final class CategoryFacade extends BaseFacade
{

	/**
	 * @param $id
	 * @return null|object
	 */
	public function getCategoryById($id)
	{
		return $this->em->find(Category::class, $id);
	}


	/**
	 * @param string $route
	 * @return mixed|null|object
	 */
	public function getCategoryByRoute($route)
	{
		return $this->em->getRepository(Category::class)->findOneBy(array('route' => $route));
	}

	/**
	 * Create new category
	 * @param ArrayHash $vals
	 */
	public function createCategory(ArrayHash $vals)
	{
		$category = new Category();
		$category->name = $vals->name;
		if (empty($vals->route))
			$category->route = ArticleFacade::createRoute($vals->name);
		else
			$category->route = ArticleFacade::createRoute($vals->route);
        $category->description = $vals->description;

        $this->em->persist($category);
    }

	public function editCategory(ArrayHash $vals)
	{
		$category = $this->em->find(Category::class, $vals->categoryId);
		if (!$category)
			throw new BadRequestException();
		$category->description = $vals->description;
		$category->name = $vals->name;
		if (!empty($vals->route))
			$category->route = ArticleFacade::createRoute($vals->route);
	}

	public function getCategoriesWithArticlesCount()
	{
		$query = new CategoryQuery();
		$query->addArticlesCount();
		return $this->em->getRepository(Category::class)->fetch($query);
	}

	public function deleteCategory($id = NULL)
	{
		if($id === NULL || is_null($category = $this->getCategoryById($id)))
			throw new InvalidArgumentException();

		$this->em->remove($category);
		$this->em->flush();
	}


}