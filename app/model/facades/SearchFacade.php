<?php
/**
 * Created by PhpStorm.
 * User: Pokoj
 * Date: 03.08.2017
 * Time: 11:49
 */

namespace App\Model\Facades;

use App\Model\Queries\SearchQuery;
use Kdyby\Doctrine\EntityManager;
use Nette\Database\Context;
use Nette\Utils\ArrayHash;

class SearchFacade
{
    /** @var EntityManager */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

	/**
	 * Opravdu zoufalé řešení ...
	 * @param $text
	 * @return ArrayHash
	 */
	public function searchAll($text)
    {
        $data = new ArrayHash();
        $data->e = new ArrayHash();
        $data->c = new ArrayHash();
        $data->e->users = $this->searchUsers($text);
        $data->e->categories = $this->searchCategories($text);
        $data->e->articles = $this->searchArticles($text);
        $data->c->users = $this->searchUsersCount($text);
        $data->c->categories = $this->searchCategoriesCount($text);
        $data->c->articles = $this->searchArticlesCount($text);
        return $data;
    }

  /*  další nezdařený pokus
  public function searchArticles($text, $limit = 5, $offset = 0)
	{
		$q = new SearchQuery();
		$q->selectArticles($text, $offset, $limit);
		return $text->em->fetch($q);
	}

    public function searchUsers($text, $limit = 5, $offset = 0)
	{
		$query = new SearchQuery();
		$query->selectUsers($text,$offset, $limit);
		return $this->em->fetch($query);
	}

	public function searchCategories($name, $limit = 5, $offset = 0)
	{
		$query = new SearchQuery();
		$query->selectCategories($name, $offset, $limit);
		return $this->em->fetch($query);
	}*/
/*
    public function searchArticles($searchText, $offset = 0, $limit = 5)
    {
        return $this->em->createQuery('
            SELECT PARTIAL a.{id, title, description, route}, COUNT (a.id) AS HIDDEN co
            FROM App\Model\Entities\Article a
            WHERE a.title LIKE :title AND a.released = 1
            GROUP BY a.title
        ')
            ->setParameter('title', "%{$searchText}%")
            ->setMaxResults($limit)
            ->setFirstResult($offset)
			->getResult();
    }

    public function searchCategories($searchText, $offset = 0, $limit = 5)
    {
        return $this->em->createQuery('
            SELECT PARTIAL c.{id, route, description, name}, COUNT(c.id) AS HIDDEN co
            FROM App\Model\Entities\Category c
            WHERE c.name LIKE :name
            GROUP BY c.name
        ')
            ->setParameter('name', "%{$searchText}%")
			->setMaxResults($limit)
            ->setFirstResult($offset)
			->getResult();
    }

    public function searchUsers($searchText, $offset = 0, $limit = 5)
    {
        return $this->em->createQuery('
            SELECT PARTIAL u.{id, username, route}, PARTIAL s.{id, description}, COUNT(u.id) AS HIDDEN co
            FROM App\Model\Entities\User u
            INNER JOIN u.settings s
            WHERE u.username LIKE :param
            GROUP BY u.username
        ')
            ->setParameter('param', "%{$searchText}%")
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getResult();
    }*/
    /*

        public function searchAll($text)
        {
            return $this->em->createQuery('
            SELECT PARTIAL user.{id,username,route}, PARTIAL settings.{id, description}, PARTIAL article.{id, title, description}, PARTIAL category.{id, name, description}
            FROM App\Model\Entities\User user
            INNER JOIN user.settings settings
            JOIN App\Model\Entities\Article article WITH user.username = article.title
            JOIN App\Model\Entities\Category category WITH user.username = category.name
            WHERE user.username = :param
        ')
                ->setParameter('param', $text)
                ->setMaxResults(5)
                ->getResult();
        }*/

	public function searchArticles($searchText, $offset = 0, $limit = 5)
	{
		return $this->em->createQuery('
            SELECT PARTIAL a.{id, title, description, route}
            FROM App\Model\Entities\Article a
            WHERE a.title LIKE :title AND a.released = 1
            GROUP BY a.title
        ')
			->setParameter('title', "%{$searchText}%")
			->setMaxResults($limit)
			->setFirstResult($offset)
			->getResult();
	}

	public function searchCategories($searchText, $offset = 0, $limit = 5)
	{
		return $this->em->createQuery('
            SELECT PARTIAL c.{id, route, description, name}
            FROM App\Model\Entities\Category c
            WHERE c.name LIKE :name
            GROUP BY c.name
        ')
			->setParameter('name', "%{$searchText}%")
			->setMaxResults($limit)
			->setFirstResult($offset)
			->getResult();
	}

	public function searchUsers($searchText, $offset = 0, $limit = 2)
	{
		return $this->em->createQuery('
            SELECT PARTIAL u.{id, username, route}, PARTIAL s.{id, description}
            FROM App\Model\Entities\User u
            INNER JOIN u.settings s
            WHERE u.username LIKE :param
            GROUP BY u.username
        ')
			->setParameter('param', "%{$searchText}%")
			->setMaxResults($limit)
			->setFirstResult($offset)
			->getResult();
	}

	public function searchArticlesCount($searchText)
	{
		return $this->em->createQuery('
            SELECT COUNT(a.id)
            FROM App\Model\Entities\Article a
            WHERE a.title LIKE :title AND a.released = 1
        ')
			->setParameter('title', "%{$searchText}%")
			->getResult();
	}

	public function searchCategoriesCount($searchText)
	{
		return $this->em->createQuery('
            SELECT COUNT(c.id)
            FROM App\Model\Entities\Category c
            WHERE c.name LIKE :name
        ')
			->setParameter('name', "%{$searchText}%")
			->getResult();
	}

	public function searchUsersCount($searchText)
	{
		return $this->em->createQuery('
            SELECT COUNT(u.id)
            FROM App\Model\Entities\User u
            WHERE u.username LIKE :param
        ')
			->setParameter('param', "%{$searchText}%")
			->getResult();
	}
}