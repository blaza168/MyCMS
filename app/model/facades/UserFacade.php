<?php
/**
 * Created by PhpStorm.
 * User: Honza
 * Date: 05.07.2017
 * Time: 10:39
 */

namespace App\Model\Facades;

use App\Model\Entities\Ban;
use App\Model\Entities\User;
use App\Model\Entities\UserSettings;
use Doctrine\ORM\EntityManager;
use Kdyby\Translation\Translator;
use Nette\InvalidArgumentException;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\IIdentity;
use Nette\Security\Passwords;
use Nette\Utils\ArrayHash;
use App\Model\Queries\UserQuery;


final class UserFacade extends BaseFacade implements IAuthenticator
{


    /** @var Translator */
    protected $translator;

    public function __construct(Translator $translator,EntityManager $em, InfoFacade $infoFacade)
    {
        $this->translator = $translator;
        $this->infoFacade = $infoFacade;
        $this->em = $em;
    }

    /**
     * @param $route
     * @return null|User
     */
    function getUserByRoute($route)
    {
        return $this->em->getRepository(User::class)->findOneBy(array('route' => $route));
    }

    function editSettings($vals, $userId)
    {
        $user = $this->getUser($userId);

        $user->settings->description = $vals->description;
        $this->em->flush();
    }

    /**
     * Function for ban user
     * @param ArrayHash $vals
     * @throws InvalidArgumentException
     */
    function banUser(ArrayHash $vals)
    {
        if(is_null($user = $this->getUser($vals->userId)))
            throw new InvalidArgumentException();

        if($vals->time === 'permanent')
            $this->user->ban->permanent = 1;
        else
            $this->user->ban->banTime = new \DateTime("+ $vals->time");

        $this->user->ban->reason = $vals->reason;

        $this->em->flush();
    }

    /**
     * @param ArrayHash $vals
     */
    public function register($vals)
    {
        $user = new User();

        $user->username = $vals->username;
        $user->password = Passwords::hash($vals->password);
        $user->route = $vals->route;
        $user->email = $vals->email;
        $user->role = User::ROLE_USER;
        $user->ip = $vals->ip;

        $ban = new Ban();
        $ban->user = $user;

        $settings = new UserSettings();
        $settings->user = $user;

        $this->em->persist($ban);
        $this->em->persist($settings);
        $this->em->persist($user);

       // $this->em->flush(); flush is in InfoFacade
    }

    /**
     * @param $id
     * @return null|User
     */
    public function getUser($id)
    {
        return $this->em->find(User::class, $id);
    }

    /**
     * Performs an authentication against e.g. database.
     * and returns IIdentity on success or throws AuthenticationException
     * @return IIdentity
     * @throws AuthenticationException
     */
    function authenticate(array $credentials)
    {
        list($username, $password) = $credentials;
    //    $user = $this->em->getRepository(User::class)->findOneBy(array('username' => $username)); Tohle smazáno, protože to vybírá spousut zbytečných dat
        $user = $this->loginUserQuery($username);

        if(!$user) throw new AuthenticationException($this->translator->translate('exception.badUsername'));

        if(!Passwords::verify($password, $user->password)) throw new AuthenticationException($this->translator->translate('exception.badPassword'));

        if($user->ban->isPermanent()) throw new AuthenticationException($this->translator->translate('exception.banPermanent'));

        if($user->ban->banTime > new \DateTime()) throw new AuthenticationException($this->translator->translate('exception.banUntil' . $user->ban->banUntil->Format('j.n.Y H:i')));

        if(Passwords::needsRehash($user->password)){
            $user->password = Passwords::hash($password);
            $this->em->flush();
        }

        return new Identity($user->id, $user->role);
    }

    private function loginUserQuery($username)
    {
        return $this->em->createQuery('
            SELECT PARTIAL user.{id, username, password, role}, PARTIAL ban.{id, permanent, banTime}
            FROM App\Model\Entities\User user
            INNER JOIN user.ban ban
            WHERE user.username = :name
        ')
            ->setParameter('name', $username)
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }
}