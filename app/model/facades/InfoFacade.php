<?php
/**
 * Created by PhpStorm.
 * User: Honza
 * Date: 06.07.2017
 * Time: 9:38
 */

namespace App\Model\Facades;

use App\Model\Entities\Information;
use App\Model\Queries\InfoQuery;
use Doctrine\ORM\EntityManager;
use Kdyby\Translation\Translator;

final class InfoFacade
{
    /** @var Translator */
    protected $translator;

    /** @var  EntityManager */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    private function newInfo($content)
    {
        $info = new Information();
        $info->content = $content;
        $info->time = new \DateTime();

        $this->em->persist($info);
        $this->em->flush();
    }

    public function getInfo($offset = 0, $limit = 10)
    {
        $query = new InfoQuery();
        $query->getLastInfo();
        return $this->em->getRepository(Information::class)
            ->fetch($query)
            ->applyPaging($offset, $limit);
    }

	/**
	 * @param string $username
	 * @param string $route
	 */
    function registeredUser($username, $route)
    {
        $this->newInfo("register_user|$username|$route" . '|info.user.registered|');
    }

	/**
	 * @param string $username
	 * @param $userId
	 * @param string $articleTitle
	 * @param $route
	 */
    function createdArticle($username, $userId ,$articleTitle, $route)
    {
        $this->newInfo("article|$username|$userId" . '|info.article.added|' . "$articleTitle|$route");
    }

	/**
	 * @param string $username
	 * @param $userId
	 * @param string $articleName
	 * @param $route
	 */
    function editedArticle($username, $userId ,$articleName, $route)
    {
        $this->newInfo("article|$username|$userId" . '|info.article.edited|' . "$articleName|$route");
    }

	/**
	 * @param string $username
	 * @param $userId
	 * @param string $categoryName
	 * @param $route
	 */
    function createdCategory($username, $userId ,$categoryName, $route)
    {
        $this->newInfo("category|$username|$userId" . '|info.category.added|' . "$categoryName|$route");
    }

	/**
	 * @param string $username
	 * @param $userId
	 * @param string $categoryName
	 * @param $route
	 */
    function editedCategory($username, $userId ,$categoryName, $route)
    {
        $this->newInfo("category|$username|$userId" . '|info.category.edited|' . "$categoryName|$route");
    }
}