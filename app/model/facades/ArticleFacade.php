<?php

namespace App\Model\Facades;

use App\Model\Entities\Article;
use App\Model\Entities\ArticleCategory;
use App\Model\Entities\Category;
use App\Model\Queries\ArticleQuery;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use App\Model\Entities\User;
use Nette\InvalidArgumentException;

final class ArticleFacade extends BaseFacade
{
    public function createArticle($vals, User $user, Category $category)
    {
        $article = new Article();
        $article->content = $vals->content;
        $article->title = $vals->title;
        $article->description = $vals->description;
        $article->route = $vals->route;
        $article->released = $user->isAdmin() ? TRUE : FALSE;
        $article->created_at = new \DateTime();
        $user->addArticle($article);
        $category->addArticle($article);

        $this->em->persist($article);

        if(!$user->isAdmin())
            $this->em->flush();
    }
    public static function createRoute($name)
    {
        $name = strtolower($name);
        return str_replace(' ', '-', $name);
    }

    public function editArticle($vals)
    {
        $article = $this->em->find(Article::class, $vals->articleId);
        if(!$article)
            throw new UniqueConstraintViolationException();
        if(!empty($vals->route))
        	$article->route = self::createRoute($vals->route);
        $article->content = $vals->content;
        $article->title = $vals->title;
        $article->description = $vals->description;
    }

    public function getCategories()
    {
        return $this->em->getRepository(Category::class)->findPairs([], 'name', [], 'id');
    }

    public function getArticleByRoute($route)
    {
        return $this->em->getRepository(Article::class)->findOneBy(array('route' => $route));
    }

    public function getArticle($id)
	{
		return $this->em->find(Article::class, $id);
	}


    public function deleteArticle($id = NULL)
	{
		if($id === NULL || is_null($article = $this->getArticle($id)))
			throw new InvalidArgumentException();

		$this->em->remove($article);
		$this->em->flush();
	}


    function getNoReleasedArticles()
    {
        $query = new ArticleQuery();
        $query->onlyNoReleased();
        return $this->em->getRepository(Article::class)->fetch($query);
    }

    function releaseArticle($id = NULL)
    {
        if($id === NULL || is_null($article = $this->em->find(Article::class, $id)))
            throw new InvalidArgumentException();
        $article->released = 1;
        $this->infoFacade->createdArticle($article->user->username, $article->user->id, $article->title, $article->route);
    }

    function getLastArticles($offset = NULL)
    {
        $query = new ArticleQuery();
        $query->onlyReleased();
        return $this->em->getRepository(Article::class)
            ->fetch($query)
            ->applyPaging($offset, 10);
    }

}