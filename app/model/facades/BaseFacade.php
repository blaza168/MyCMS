<?php
/**
 * Created by PhpStorm.
 * User: Honza
 * Date: 05.07.2017
 * Time: 10:39
 */

namespace App\Model\Facades;


use Kdyby\Doctrine\EntityManager;
use Kdyby\Translation\Translator;

abstract class BaseFacade
{
    /** @var  EntityManager */
    protected $em;

    /** @var InfoFacade */
    protected $infoFacade;

    /**
     * BaseFacade constructor.
     * @param EntityManager $em
     * @param Translator $translator
     */
    public function __construct(EntityManager $em, InfoFacade $infoFacade )
    {
        $this->infoFacade = $infoFacade;
        $this->em = $em;
    }
}