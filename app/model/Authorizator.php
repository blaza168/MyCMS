<?php
/**
 * Created by PhpStorm.
 * User: Honza
 * Date: 05.07.2017
 * Time: 10:29
 */

namespace App\Model;


use Nette\Security\IAuthorizator;
use Nette\Security\privilege;
use Nette\Security\role;
use Nette\Security\Permission;

class Authorizator implements IAuthorizator
{

    /**
     * Performs a role-based authorization.
     * @param  string  role
     * @param  string  resource
     * @param  string  privilege
     * @return bool
     */
    function isAllowed($role, $resource, $privilege)
    {
        if($role === 'admin')
            return true;
        else if ($role === 'mod')
        {
            if($resource === 'article' && $privilege === 'write')
                return true;
            return false;
        }
        else
            return false;
    }

    /**
     * @return Permission
     */
    static function create()
    {
        $acl = new Permission();

        $acl->addRole('guest');
        $acl->addRole('user', 'guest');
        $acl->addRole('mod', 'user');
        $acl->addRole('admin', 'mod');

        $acl->addResource('user');
        $acl->addResource('article');
        $acl->addResource('category');


        $acl->allow('mod', 'article', 'write');
        $acl->allow('admin', Permission::ALL);

        return $acl;
    }
}