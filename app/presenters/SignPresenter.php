<?php

namespace App\Presenters;

use App\Model\Facades\ArticleFacade;
use App\Model\Facades\InfoFacade;
use App\Forms\SignFormFactory;

final class SignPresenter extends BasePresenter
{
    /** @var SignFormFactory */
    protected $signFormFactory;

    /** @var InfoFacade */
    protected $infoFacade;


    /**
     * @param SignFormFactory $signFormFactory
     * @param InfoFacade $infoFacade
     */
    function injectDependences(SignFormFactory $signFormFactory, InfoFacade $infoFacade)
    {
        $this->infoFacade = $infoFacade;
        $this->signFormFactory = $signFormFactory;
    }

    function createComponentSignInForm()
    {
        $form = $this->signFormFactory->signInForm();
        $form->onSuccess[] = function (){
            $this->flashMessage($this->translator->translate('mess.logged'), 'success');
          $this->redirect('Homepage:');
        };
        return $form;
    }

    public function createComponentSignUpForm()
    {
        $form = $this->signFormFactory->signUpForm();
        $form->onSuccess[] = function ($form, $vals){
            $this->flashMessage($this->translator->translate('mess.registered'), 'success');
            $this->redirect('User:detail', ArticleFacade::createRoute($vals->username));
        };
        return $form;
    }
}
