<?php

namespace App\Presenters;


use App\Forms\UserFormFactory;
use App\Model\Facades\UserFacade;

final class UserPresenter extends BasePresenter
{

    /** @var UserFacade */
    protected $userFacade;

    /** @var UserFormFactory */
    private $userFormFactory;

    /**
     * UserPresenter constructor.
     * @param UserFacade $userFacade
     */
    public function __construct(UserFacade $userFacade, UserFormFactory $userFormFactory)
    {
        $this->userFormFactory = $userFormFactory;
        $this->userFacade = $userFacade;
    }

    function actionSettings()
    {
        if(!$this->user->isLoggedIn())
            $this->redirect('Sign:in');
        $this['settingForm']->setDefaults(array(
            'description' => $this->userEntity->settings->description,
        ));
    }

    function renderDetail($route)
    {
        $user = $this->userFacade->getUserByRoute($route);
        if (!$user)
            $this->error();
        $this->template->userEntity = $user;
    }

    function createComponentSettingForm()
    {
        $form = $this->userFormFactory->settingsForm();
        $form->onSuccess[] = function (){
            $this->flashMessage('mess.user.settingsEdited', 'success');
            $this->redirect(':detail', $this->userEntity->route);
        };
        return $form;
    }

    function actionBan($id)
    {
        if(!$this->user->isAllowed('user', 'ban'))
            $this->error(403);
        $this['banForm']->setDefaults(array(
            'userId' => $id
        ));
    }

    function createComponentBanForm()
    {
        $form = $this->userFormFactory->banForm();
        $form->onSuccess[] = function (){
            $this->flashMessage($this->translator->translate('mess.user.banned'), 'success');
            $this->redirect('Homepage:');
        };
        return $form;
    }

    function actionEdit($id)
    {
        if(!$this->user->isInRole('admin'))
            $this->error(403);
    }
}