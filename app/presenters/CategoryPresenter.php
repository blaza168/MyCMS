<?php

namespace App\Presenters;

use App\Forms\CategoryFormFactory;
use App\Model\Facades\CategoryFacade;
use Doctrine\Common\Collections\Criteria;
use Nette\InvalidArgumentException;

final class CategoryPresenter extends BasePresenter
{

    /**
     * @var CategoryFormFactory
     */
    protected $categoryFormFactory;

    /** @var  CategoryFacade */
    protected $categoryFacade;

    /**
     * @param CategoryFormFactory $categoryFormFactory
     * @param CategoryFacade $categoryFacade
     */
    function injectDependences(CategoryFormFactory $categoryFormFactory, CategoryFacade $categoryFacade)
    {
        $this->categoryFormFactory = $categoryFormFactory;
        $this->categoryFacade = $categoryFacade;
    }

    function actionCreate()
    {
        if(!$this->user->isAllowed('category', 'create'))
            $this->redirect('Homepage:');
    }

    function createComponentCreateCategoryForm()
    {
        $form = $this->categoryFormFactory->createCategoryForm();
        $form->onSuccess[] = function (){
            if ($this->isLinkCurrent(':create')){
                $this->flashMessage($this->translator->translate('mess.category.created'), 'success');
                $this->redirect('Homepage:');
            } else{
                $this->flashMessage($this->translator->translate('mess.category.edited', 'success'));
                $this->redirect('Homepage:');
            }
        };
        return $form;
    }

    function actionEdit($route){
        $category = $this->categoryFacade->getCategoryByRoute($route);
        if(!$this->user->isAllowed('category', 'edit'))
            $this->redirect('Homepage:');
        if(!$category){
            $this->flashMessage($this->translator->translate('mess.category.doesntExists'));
            $this->redirect('Homepage:');
        }
        $this['createCategoryForm']->setDefaults(array(
            'name' => $category->name,
            'categoryId' => $category->id,
            'description' => $category->description
        ));
    }

    function renderDetail($route)
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('released', 1));
        $category = $this->categoryFacade->getCategoryByRoute($route);
        if(!$category){
            $this->flashMessage($this->translator->translate('mess.category.doesntExists'));
            $this->redirect('Homepage:');
        }
        $category->articles = $category->articles->matching($criteria);
        $this->template->category = $category;
    }

    function handleDelete($id)
	{
		if($this->user->isAllowed('category','delete'))
		{
			try{
				$this->categoryFacade->deleteCategory($id);
				$this->flashMessage($this->translator->translate('mess.category.deleted'), 'success');
			} catch (InvalidArgumentException $e){
				$this->flashMessage($this->translator->translate('mess.category.notFound'), 'danger');
			}
		}
		else
			$this->flashMessage($this->translator->translate('exception.permission'), 'danger');
		$this->redirect('Homepage:');
	}

    function renderList()
    {
        $this->template->categories = $this->categoryFacade->getCategoriesWithArticlesCount();
    }
}