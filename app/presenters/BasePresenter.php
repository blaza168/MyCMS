<?php

namespace App\Presenters;

use App\Forms\searchFormFactory;
use Nette;
use App\Model;
use Kdyby\Translation\Translator;
use Nette\Bridges\ApplicationLatte\Template;
use App\Model\Entities\User as UserEntity;
use App\Model\Facades\UserFacade;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    /** @var  UserEntity */
    protected $userEntity;

    /** @persistent string|null web language   */
    public $locale;

    /** @var  Translator */
    protected $translator;

    /** @var  UserFacade */
    protected $userFacade;

    /** @var searchFormFactory */
    protected $searchFormFactory;

    public function injectAtributes(searchFormFactory $searchFormFactory,Translator $translator, UserFacade $userFacade)
    {
        $this->searchFormFactory = $searchFormFactory;
        $this->translator = $translator;
        $this->userFacade = $userFacade;
    }

    public function startup()
    {
        parent::startup();
        if($this->user->isLoggedIn())
            $this->userEntity = $this->userFacade->getUser($this->user->getId());
        else{
            $this->userEntity = new UserEntity();
            $this->userEntity->role = UserEntity::ROLE_USER;
        }
        $this->template->userEntity = $this->userEntity;
    }

    /**
     * @inheritdoc
     */
    protected function createTemplate()
    {
        /** @var Template $template Latte template for current presenter */
        $template = parent::createTemplate();
        $this->translator->createTemplateHelpers()
            ->register($template->getLatte());
        return $template;
    }

    public function handleLogout()
    {
        $this->user->logout();
        $this->flashMessage($this->translator->translate('com.base.logout'), 'success');
        $this->redirect('Homepage:');
    }

    public function createComponentSearchForm()
    {
        $form = $this->searchFormFactory->searchForm();
        $form->onSuccess[] = function ($form,$vals){
            $this->redirect('Homepage:search', ['text' => $vals->text]);
        };
        return $form;
    }

}
