<?php

namespace App\Presenters;

use App\Model\Facades\InfoFacade;
use App\Model\Facades\SearchFacade;
use Nette\Utils\ArrayHash;

class HomepagePresenter extends BasePresenter
{

    /** @var InfoFacade */
    protected $infoFacade;

    /** @var SearchFacade */
    protected $searchFacade;

	/**
	 * @param InfoFacade $infoFacade
	 */
	function injectDependences(SearchFacade $searchFacade,InfoFacade $infoFacade)
    {
        $this->searchFacade = $searchFacade;
        $this->infoFacade = $infoFacade;
    }

    public function renderDefault()
    {
        $this->template->informations = $this->infoFacade->getInfo();
    }

    function renderSearch($text, $count, $entity)
    {
		$result = $this->searchFacade->searchAll($text);
		$this->template->count = $result->c;
		$this->template->text = $text;

		$this->template->result = $result->e;
        if($this->isAjax()){
        	$this->template->result = new ArrayHash();
			if($entity === 'user')
				$this->template->result->users = $this->searchFacade->searchUsers($text, 0, $count + 10);
			else if($entity === 'category')
				$this->template->result->categories = $this->searchFacade->searchCategories($text, 0, $count + 10);
			else
				$this->template->result->articles = $this->searchFacade->searchArticles($text, 0, $count + 10);
		}
    }

  /*  function handleMore($entity, $text, $count)
    {
        switch ($entity)
        {
            case 'user':
            	$this->template->result = new ArrayHash();
            	$this->template->result->users = $this->searchFacade->searchUsers($text, 0, $count + 10);
            	$this->redrawControl('users');
                break;
            case 'article':
				$this->template->result = new ArrayHash();
				$this->template->result->categories = $this->searchFacade->searchCategories($text, 0, $count + 10);
				$this->redrawControl('categories');
                break;
            case 'category':
				$this->template->result = new ArrayHash();
				$this->template->result->articles = $this->searchFacade->searchArticles($text, 0, $count + 10);
				$this->redrawControl('articles');
                break;
        }
		$this->template->text = $text;
    }*/
}
