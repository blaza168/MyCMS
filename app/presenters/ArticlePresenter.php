<?php
/**
 * Created by PhpStorm.
 * User: Honza
 * Date: 09.07.2017
 * Time: 16:10
 */

namespace App\Presenters;


use App\Forms\ArticleFormFactory;
use App\Model\Facades\ArticleFacade;
use Nette\InvalidArgumentException;
use Nette\Neon\Exception;
use Texy\Texy;

final class ArticlePresenter extends BasePresenter
{
    /** @var  ArticleFormFactory */
    protected $articleFormFactory;

    /** @var  ArticleFacade */
    protected $articleFacade;

    /** @var  Texy */
    protected $texy;

    function injectDependencies(ArticleFacade $articleFacade, ArticleFormFactory $articleFormFactory, Texy $texy)
    {
        $this->texy = $texy;
        $this->articleFormFactory = $articleFormFactory;
        $this->articleFacade = $articleFacade;
    }

    function createComponentCreateArticleForm()
    {
        $form = $this->articleFormFactory->createArticleForm();
        $form->onSuccess[] = function (){
            $this->flashMessage($this->translator->translate('mess.article.created'), 'success');
            $this->redirect('Homepage:');
        };
        return $form;
    }

    function actionCreate()
    {
        if(!$this->user->isAllowed('article', 'write'))
            $this->redirect('Homepage:');
    }


    function actionEdit($route)
    {
        if(!$this->user->isAllowed('article', 'edit'))
            $this->redirect('Homepage:');
        $article = $this->articleFacade->getArticleByRoute($route);
        if(!$article)
            $this->redirect(':create');
        $this['editArticleForm']->setDefaults(array(
            'title' => $article->title,
            'route' => $article->route,
            'articleId' => $article->id,
            'description' => $article->description
        ));
        $this->template->text = $article->content;
    }

    function renderCreate()
    {
        if(!$this->isAjax())
        {
            $this->template->edit = "true";
            $this->template->text = '';
        }
    }

    function renderEdit()
    {
        if(!$this->isAjax())
        {
            $this->template->edit = "true";
        }
    }

    function createComponentEditArticleForm()
    {
        $form = $this->articleFormFactory->createArticleForm();
        $form->onSuccess[] = function (){
            $this->flashMessage($this->translator->translate('mess.article.edited'), 'success');
            $this->redirect('Homepage:');
        };
        return $form;
    }

    function beforeRender()
    {
        $this->template->addFilter('texy', function ($val){
            return $this->texy->process($val);
        });
    }

    function renderDetail($route)
    {
        $article = $this->articleFacade->getArticleByRoute($route);
        if(!$article || ($article->released === 0 && !$this->user->isInRole('admin')))
            $this->error();
        $this->template->article = $article;
    }

    function handleDelete($id)
	{
		if($this->user->isAllowed('article','delete'))
		{
			try{
				$this->articleFacade->deleteArticle($id);
				$this->flashMessage($this->translator->translate('mess.article.deleted'), 'success');
			} catch (InvalidArgumentException $e){
				$this->flashMessage($this->translator->translate('mess.article.notFound'), 'danger');
			}
		}
		else
			$this->flashMessage($this->translator->translate('exception.permission'), 'danger');
		$this->redirect('Homepage:');
	}

	function handleRelease($id)
    {
        if($this->user->isInRole('admin'))
        {
            try{
                $this->articleFacade->releaseArticle($id);
                $this->flashMessage($this->translator->translate('article.released'), 'success');
            } catch(InvalidArgumentException $e){
                $this->flashMessage($this->translator->translate('article.DoesntExists'), 'danger');
            }
        }
        $this->redirect('Homepage:');
    }

    function handleChange($edit, $text)
    {
        $this->template->text = $text;
        $this->template->edit = $edit;
        $this->redrawControl('buttons');
        $this->redrawControl('show');
    }

    function renderList()
	{
		$this->template->articles = $this->articleFacade->getLastArticles();
	}

	function renderAdmin()
    {
        if(!$this->user->isInRole('admin'))
            $this->error(403);

        $this->template->articles = $this->articleFacade->getNoReleasedArticles();

    }
}