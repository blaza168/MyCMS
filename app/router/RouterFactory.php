<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;


class RouterFactory
{
    use Nette\StaticClass;

    /**
     * @return Nette\Application\IRouter
     */
    public static function createRouter()
    {
        $router = new RouteList;
        $router[] = new Route('[<locale=cs cs|en>/]list', 'Category:list');
        $router[] = new Route('[<locale=cs cs|en>/]create', 'Article:create');
        $router[] = new Route('[<locale=cs cs|en>/]profile/<route>', 'User:detail');
        $router[] = new Route('[<locale=cs cs|en>/]search/<text>[/<count=0>/<entity>]', 'Homepage:search');
        $router[] = new Route('[<locale=cs cs|en>/]createCategory', 'Category:create');
        $router[] = new Route('[<locale=cs cs|en>/]banPage', 'User:ban');
	//	$router[] = new Route('[<locale=cs cs|en>/]edit/<route>', 'Category:edit');
		$router[] = new Route('[<locale=cs cs|en>/]category/<route>', 'Category:detail');
		$router[] = new Route('[<locale=cs cs|en>/]article/detail/<route>', 'Article:detail');
        $router[] = new Route('[<locale=cs cs|en>/]<presenter>/<action>[/<route>]', 'Homepage:default');
        return $router;
    }

}
