function generateURL() {
    var url = $('#title').val();
    url = url.replace(' ', '-');
    return url.toLowerCase();
}

$(
    $('#route').click(function () {
        if($(this).is(':checked')){
            $('#route-input').val(generateURL());
        }
    }),
    $('#title').change(function () {
        if($('#route').is(':checked')){
            $('#route-input').val(generateURL());
        }
    })
)