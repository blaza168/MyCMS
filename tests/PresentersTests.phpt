<?php
namespace Test;

use Nette;
use Tester;
use Tester\Assert;

require __DIR__ . '/bootstrap.php';

/**
 * Class PresentersTests for action|handle|ajax tests
 * @package Test
 * @testCase
 */
class PresentersTests extends Tester\TestCase
{
    use \Testbench\TPresenter;

    public function testHomepage()
    {
        $this->checkAction('Homepage:default');
        $this->checkAction('Homepage:search', ['text' => 'a']);
    }

}
(new PresentersTests())->run();