<?php

require __DIR__ . '/../vendor/autoload.php';

Tester\Environment::setup();
Testbench\Bootstrap::setup(__DIR__ . '/../temp', function (\Nette\Configurator $configurator) {
    $configurator->createRobotLoader()->addDirectory([
        __DIR__ . '/../app',
    ])->register();

    $configurator->addParameters([
        'appDir' => __DIR__ . '/../app',
    ]);

    $configurator->setTempDirectory(__DIR__);

    $configurator->addConfig(__DIR__ . '/../app/config/config.neon');
    $configurator->addConfig(__DIR__ . '/tests.neon');
});

$configurator = new Nette\Configurator;
$configurator->setDebugMode(FALSE);
$configurator->setTempDirectory(__DIR__ . '/../temp');
$configurator->createRobotLoader()
	->addDirectory(__DIR__ . '/../app')
	->register();

$configurator->setTempDirectory(__DIR__);

$configurator->addConfig(__DIR__ . '/../app/config/config.neon');
$configurator->addConfig(__DIR__ . '/../app/config/config.local.neon');
return $configurator->createContainer();

/*
require __DIR__ . '/../vendor/autoload.php';

Testbench\Bootstrap::setup(__DIR__ . '/../temp', function (\Nette\Configurator $configurator) {
    $configurator->createRobotLoader()->addDirectory([
        __DIR__ . '/../app',
    ])->register();

    $configurator->addParameters([
        'appDir' => __DIR__ . '/../app',
    ]);

    $configurator->setTempDirectory(__DIR__);

    $configurator->addConfig(__DIR__ . '/../app/config/config.neon');
    $configurator->addConfig(__DIR__ . '/tests.neon');
});
*/