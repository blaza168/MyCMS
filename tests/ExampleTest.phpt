<?php

namespace Test;

use Nette;
use Testbench\TPresenter;
use Tester;
use Tester\Assert;

$container = require __DIR__ . '/bootstrap.php';

/**
 * https://regex101.com/
 * Class ExampleTest
 * @package Test
 * @testCase
 */
class ExampleTest extends Tester\TestCase
{

    use TPresenter;

    /** @var Nette\DI\Container */
	private $container;


	function __construct(Nette\DI\Container $container)
	{
		$this->container = $container;
	}


	function setUp()
	{

	}

    private function articleData()
    {
        return [
            'title' => 'TestingTitle' . rand(1,500),
            'content' => 'Article´s content',
            'description' => 'article´s description from akceptance testing',
            'category' => '7'
        ];
    }


	function testForm()
	{
        $this->logIn(26, 'admin');
        $this->checkForm('Article:create', 'createArticleForm', $this->articleData(), '/\?_fid=[a-zA-Z0-9]+');
        $this->checkForm('Homepage:default', 'searchForm', ['text' => 'a'], '/search/a');
	}
}

$test = new ExampleTest($container);
$test->run();
