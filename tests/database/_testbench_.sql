-- Adminer 4.3.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

-- CREATE DATABASE `_testbench_`;
USE `_testbench_`;

DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` varchar(150) COLLATE utf8mb4_czech_ci NOT NULL,
  `content` varchar(10000) COLLATE utf8mb4_czech_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `released` tinyint(1) NOT NULL,
  `title` varchar(20) COLLATE utf8mb4_czech_ci NOT NULL,
  `route` varchar(40) COLLATE utf8mb4_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `route` (`route`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `articles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

TRUNCATE `articles`;
INSERT INTO `articles` (`id`, `user_id`, `category_id`, `description`, `content`, `created_at`, `released`, `title`, `route`) VALUES
  (2,	26,	6,	'Popis prvního článku',	'Tenhle článek UNIKÁTNÍ! Jde o první článek, který obsahuje normální routu! Snad se přidá i první článek, který bude obsahovat texy syntax.\n-Upraveno',	'2017-07-24 20:35:04',	1,	'První článek',	'první-článek'),
  (3,	26,	7,	'- popissss',	'- test cache',	'2017-07-25 19:04:14',	1,	'Druhý článek',	'druhý-článek'),
  (4,	26,	7,	'asdasd',	'asdasd',	'2017-07-25 19:05:29',	1,	'asdasd',	'asdasd'),
  (6,	26,	7,	'asdasdasdasd',	'sadasdasdasd',	'2017-07-30 21:10:01',	1,	'asdsadas',	'asdsadas'),
  (7,	26,	7,	'sefsefsef',	'sefsefsefsef',	'2017-07-30 21:13:29',	1,	'sefdsfsef',	'sefdsfsef'),
  (8,	26,	7,	'Popis článku o testu odkazu',	'Tento článek testuje, zda se vygeneruje odkaz v Homepage:',	'2017-07-31 12:10:19',	1,	'Test odkazu',	'test-odkazu'),
  (9,	26,	7,	'problém flash messanges',	'asdasadasd\nEDIT: Problém vyřešen',	'2017-07-31 12:28:49',	1,	'problém ass',	'problém-ass');

DROP TABLE IF EXISTS `bans`;
CREATE TABLE `bans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permanent` tinyint(1) NOT NULL DEFAULT '0',
  `ban_until` datetime DEFAULT NULL,
  `reason` varchar(500) COLLATE utf8mb4_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `bans_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

TRUNCATE `bans`;
INSERT INTO `bans` (`id`, `user_id`, `permanent`, `ban_until`, `reason`) VALUES
  (1,	23,	0,	NULL,	NULL),
  (2,	24,	0,	NULL,	NULL),
  (3,	25,	0,	NULL,	NULL),
  (4,	26,	0,	NULL,	NULL);

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) COLLATE utf8mb4_czech_ci NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_czech_ci NOT NULL,
  `route` varchar(40) COLLATE utf8mb4_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `route` (`route`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

TRUNCATE `categories`;
INSERT INTO `categories` (`id`, `description`, `name`, `route`) VALUES
  (7,	'- test cache',	'Druhý článek',	'druhý-článek'),
  (9,	'asdasdasd',	'Kategorie ke smazání',	'asdasd');

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content` varchar(300) COLLATE utf8mb4_czech_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `article_id` (`article_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`),
  CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

TRUNCATE `comments`;

DROP TABLE IF EXISTS `info`;
CREATE TABLE `info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(150) COLLATE utf8mb4_czech_ci NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

TRUNCATE `info`;
INSERT INTO `info` (`id`, `content`, `datetime`) VALUES
  (19,	'article|admin|26|info.article.added|Test odkazu|test-odkazu',	'2017-07-31 12:10:19'),
  (20,	'article|admin|26|info.article.added|problém ass|problém-ass',	'2017-07-31 12:28:49'),
  (21,	'article|admin|26|info.article.edited|problém ass|problém-ass',	'2017-07-31 13:49:00'),
  (22,	'category|admin|26|info.category.added|Kategorie ke smazání|',	'2017-07-31 22:39:56'),
  (23,	'category|admin|26|info.category.added|Kategorie ke smazání|asdasd',	'2017-07-31 22:45:40'),
  (24,	'category|admin|26|info.category.added|Další mazací kategor|další-mazací kategor',	'2017-07-31 22:48:12'),
  (25,	'article|admin|26|info.article.added|adfsdfsdfsdf|adfsdfsdfsdf',	'2017-07-31 22:48:29');

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `description` varchar(200) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `settings_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

TRUNCATE `settings`;
INSERT INTO `settings` (`id`, `user_id`, `description`) VALUES
  (8,	23,	NULL),
  (9,	24,	NULL),
  (10,	25,	NULL),
  (11,	26,	NULL);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `role` varchar(5) COLLATE utf8_czech_ci NOT NULL,
  `ip` varchar(17) COLLATE utf8_czech_ci NOT NULL,
  `route` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

TRUNCATE `users`;
INSERT INTO `users` (`id`, `username`, `password`, `email`, `role`, `ip`, `route`) VALUES
  (23,	'blaza168',	'$2y$10$GzHe3zFaFpKEZoScGg5Jf.h9t8jDcoPkQY9MukJ8ndU13TEsGwgVi',	'MyCMS@seznam.cz',	'user',	'127.0.0.1',	''),
  (24,	'asdassd',	'$2y$10$hC9nN.9EfrTQ53j8pni9FufW6ows1KEZsMC8/NpoBxps7XxyJ2l8.',	'asdsa@asdsad.cz',	'user',	'127.0.0.1',	''),
  (25,	'asdasd',	'$2y$10$478CPbX.529JlX6tdG6oyO3e3CPXLk2TggOQDgVp9eqcVlIIw7Jnq',	'asdasd@asdasdas.cz',	'user',	'127.0.0.1',	''),
  (26,	'admin',	'$2y$10$niFCPGjB4fLMA2/R7uZ1qutl2bGZuqPkKIH5Pj.xZXQGv2.W.Lhzy',	'admin@mycms.cz',	'admin',	'127.0.0.1',	'');

-- 2017-08-05 12:52:31
